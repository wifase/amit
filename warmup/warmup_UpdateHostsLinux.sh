# Update the hosts file to block domains we don't want traffic going to.

while IFS="" read -r p || [ -n "$p" ]
do
    if  [[ ! $p == \#* ]]
    then
        echo $p >> /etc/hosts
    fi
done < /home/runner/warmup/warmup_hosts.txt

# due to reusing the host name from the factory disk across all VMs. see https://github.com/actions/virtual-environments/issues/3185
echo -e "$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1) $(hostname -f) $(hostname -s)" >> /etc/hosts

# this only needs to be done once in the lifetime of this machine
echo "" > /home/runner/warmup/warmup_hosts.txt
