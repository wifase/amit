#!/bin/bash

# Start cgroup rules daemon
if ! pgrep --exact "cgrulesengd" > /dev/null
then
    cgrulesengd
fi
# Must run after reboot so kernel has swapaccount=1
cgconfigparser --load=/etc/cgconfig.conf

pushd '/home/runner/runners'
for agentFolder in *; do
    if [[ -d $agentFolder ]]; then
        if [[ -f $agentFolder/bin/Agent.Listener ]]
        then
            $agentFolder/bin/Agent.Listener warmup &
            $agentFolder/bin/Agent.Worker spawnclient 0 0 &
        else
            $agentFolder/bin/Runner.Listener warmup &
            $agentFolder/bin/Runner.Worker spawnclient 0 0 &
        fi
    fi
done
wait
popd
