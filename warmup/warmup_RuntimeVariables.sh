user=runner
user_id=$(id -u $user)

echo "Setting up XDG_RUNTIME_DIR"
echo "d /run/user/$user_id 0700 $user $user" >> /tmp/1001-users.conf
sudo systemd-tmpfiles --create /tmp/1001-users.conf
echo "XDG_RUNTIME_DIR=/run/user/$user_id" | sudo tee -a /etc/environment
rm /tmp/1001-users.conf
